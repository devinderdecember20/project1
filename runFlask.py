from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello world from flask";

@app.route('/test')
def test():
    return "Hello world from test";

if __name__ == '__main__':
    app.run(port=4049,debug=True)